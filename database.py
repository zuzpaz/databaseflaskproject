from bson.objectid import ObjectId
from pymongo import MongoClient


class GameQuery:
    def __init__(self):
        self.__find_query = {}

    def clearQuery(self):
        self.__find_query = {}

    def shopName(self, site):
        if 'site.name' not in self.__find_query:
            self.__find_query['site.name'] = {'$all': []}
        self.__find_query['site.name']['$all'].append(site)

    def gameId(self, game_id):
        self.__find_query['_id'] = ObjectId(game_id)

    def priceMin(self, min_price):  # if in any shop that game have such price
        if 'site.price' not in self.__find_query:
            self.__find_query['site.price'] = {}
        self.__find_query['site.price']['$gt'] = float(min_price)

    def priceMax(self, max_price):  # if in any shop that game have such price
        if 'site.price' not in self.__find_query:
            self.__find_query['site.price'] = {}
        self.__find_query['site.price']['$lt'] = float(max_price)

    def availability(self, availability):  # if in any shop that game is available
        if 'site.availability' not in self.__find_query:
            self.__find_query['site.availability'] = {'$in': []}
        self.__find_query['site.availability']['$in'].append(availability)

    def category(self, category): # if any of that category is in game
        if 'category' not in self.__find_query:
            self.__find_query['category'] = {'$all': []}
        self.__find_query['category']['$all'].append(category)

    def get(self):
        return self.__find_query


class GameResult:
    # argument - cursor, result - games search result
    def __init__(self, cursor):
        self.cursor = cursor

    def getGamesResult(self):
        return [self.parseGame(i) for i in self.cursor]  # list of games dictionaries

    def getOneGameResult(self):
        return self.parseGame(self.cursor)[0]

    def parseGame(self, cursor_element):  # helping method if object is no dict
        return cursor_element

    def countTotal(self):  # count games
        return self.cursor.count()


class Database:
    def __init__(self, collection):
        self.collection = MongoClient('mongodb://localhost:27017/')['boardgames'][collection]

    def getGames(self, query, page_num=1, page_size=1):
        # based on the GameQuery instance, formulate a query
        # and return instance GamesResult called on the cursor
        skip_games = page_size * (page_num - 1)
        cursor = self.collection.find(query.get()).skip(skip_games).limit(page_size)

        return GameResult(cursor)

    def getAllTypes(self, key):
        return self.collection.distinct(key)  # list all distinct values for key

    def countTotal(self):  # count games
        return self.collection.count()

