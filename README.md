**DatabaseFlaskProject**

---

## Program description

The site displays board games from a database that can be filtered based on category, availability, price, etc. It also shows a single game page with detailed information.

Used technologies:

- Flask 
- Bootstrap 
- CSS
- Jinja 
- MongoDB

---
