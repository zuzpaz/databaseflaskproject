import bson
from flask import render_template, Flask, request
import database

app = Flask(__name__, template_folder='templates/page')
app.static_folder = 'static'


@app.route('/', methods=['GET'])
def games_site():
    q = database.GameQuery()
    search_query = request.args
    if 'site' in search_query:
        for i in search_query.getlist('site'):
            q.shopName(i)
    if 'availability' in search_query:
        for i in search_query.getlist('availability'):
            q.availability(i)
    if 'category' in search_query:
        for i in search_query.getlist('category'):
            q.category(i)
    if 'min_price' in search_query and search_query['min_price'] is not '':
        q.priceMin(search_query['min_price'])
    if 'max_price' in search_query and search_query['max_price'] is not '':
        q.priceMax(search_query['max_price'])
    res = database.Database('games_collection')  # res is instance Database with argument collection
    stock = res.getAllTypes('site.availability')
    shops = res.getAllTypes('site.name')
    categories = res.getAllTypes('category')
    categories_searched = search_query.getlist('category')
    active_page = request.args.get('page', default=1, type=int)
    per_page = 9
    games = res.getGames(q, active_page, per_page)  # called method getGames returns instance GameResult() with games from q
    total = games.countTotal()
    number_of_pages = - (-total // per_page)  # ceil division
    games_result = games.getGamesResult()  # method getGamesResult called on instance GamesResult returns list of games dicts

    def go_to_page(page):
        args = request.args.copy()
        args['page'] = page
        return {k: v for k, v in args.items()}

    app.jinja_env.globals.update(go_to_page=go_to_page)  # function can be used in jinja template
    return render_template('index.html', games_result=games_result, shops=shops, stock=stock, categories=categories,
                           active_page=active_page, number_of_pages=number_of_pages, search_query=search_query,
                           categories_searched=categories_searched)


@app.route('/<game_id>')
def game_site(game_id):
    q = database.GameQuery()
    try:
        q.gameId(game_id)
    except bson.errors.InvalidId:
        return render_template('error.html')
    res = database.Database('games_collection')
    game = res.getGames(q)
    game_result = game.getOneGameResult()
    return render_template('game.html', game_result=game_result, game_id=game_id)
